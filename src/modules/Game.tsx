import axios from 'axios'

import { SERVER_HOST, SERVER_PORT } from '../common/config'

const host = `http://${SERVER_HOST}:${SERVER_PORT}`

export const Game = {
  draw: () => {
    return axios
      .post(`${host}/play`, { command: 'draw' })
      .then(result => result)
      .catch(error => {
        // tslint:disable-next-line:no-console
        console.log('EXecption in Game.draw: ', error)
      })
  },
  endTurn: () => {
    return axios
      .post(`${host}/play`, { command: 'end' })
      .then(result => result)
      .catch(error => {
        // tslint:disable-next-line:no-console
        console.log('EXecption in Game.endTurn: ', error)
      })
  },
  showHistory: (playerName: string) => {
    return axios
      .post(`${host}/show-history`, { PlayerName: playerName })
      .then(result => result)
      .catch(error => {
        // tslint:disable-next-line:no-console
        console.log(`Exception found in game.showHistory: `, error)
      })
  },
  startGame: (playerName: string) => {
    return axios
      .post(`${host}/start`, { Name: playerName })
      .then(result => result)
      .catch(error => {
        // tslint:disable-next-line:no-console
        console.log('Exception Game.startGame: ', error)
      })
  }
}
