import {
  DISPLAY_TABLE,
  END_TURN,
  GAME_STATUS,
  SET_CARDS_ON_HAND,
  SET_CURRENT_VALUE,
  SET_PLAYER_NAME,
  START_GAME
} from '../common/action_types'

// Interfaces
import { Action, State } from '../interfaces'

const initialState = { player: { name: '' }, cardsOnHand: [], currentvalue: 0, status: false, history: [] }

const reducer = (state = initialState, action: Action): State => {
  switch (action.type) {
    case DISPLAY_TABLE:
      return { ...state, history: action.payload }
    case END_TURN:
      return initialState
    case GAME_STATUS:
      return { ...state, status: action.payload }
    case SET_CARDS_ON_HAND:
      return { ...state, cardsOnHand: action.payload }
    case SET_CURRENT_VALUE:
      return { ...state, currentValue: action.payload }
    case SET_PLAYER_NAME:
      return { ...state, player: action.payload.playerName }
    case START_GAME:
      return state
    default:
      return state
  }
}

export default reducer
