import { Box, Container } from '@material-ui/core'
import React from 'react'
import { connect } from 'react-redux'

// Components
import Display from '../display'
import Header from '../header'

// Actions
import { startGame } from '../../actions/game'
import { draw, endTurn } from '../../actions/player'

// Interfaces
import { State } from '../../interfaces'

class Dashboard extends React.Component {
  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    document.addEventListener(
      'keydown',
      () => {
        switch (event.keyCode) {
          case 13:
            this.props.onKeypressEnter(this.props.player.name)
            break
          case 16:
            this.props.onKeyPressShift()
            break
          case 88:
            this.props.onKeyPressX(this.props.player.name)
            break
          default:
            break
        }
      },
      false
    )
  }

  render() {
    return (
      <Box>
        <Container>
          <Header />
          <Display />
        </Container>
      </Box>
    )
  }
}

const mapStateToProps = (state: State) => ({
  player: state.player
})

const mapDispatchToProps = (dispatch: any) => {
  return {
    onKeypressEnter: (name: string) => {
      dispatch(startGame(name))
    },
    onKeyPressShift: () => {
      dispatch(draw())
    },
    onKeyPressX: (name: string) => {
      // tslint:disable-next-line:no-console
      dispatch(endTurn(name))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard)
