import { TableCell } from '@material-ui/core'
import React from 'react'

const Summary = props => {
  const cards = props.cards

  let display

  if (cards.length > 0) {
    let container: string[]
    container = []
    cards.map(card => {
      if (card && card.Value !== undefined && card.Face !== undefined) {
        const stringdDisplay = `${card.Value} -> ${card.Face}`
        container.push(stringdDisplay)
      }

      // tslint:disable-next-line:no-console
      console.log(`CONTAINER: `, container)

      display = container.join('||')
    })
  } else {
    display = 'none'
  }

  return <span>{display}</span>
}

export default Summary
