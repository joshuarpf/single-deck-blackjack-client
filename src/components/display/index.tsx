import React from 'react'
import { useSelector } from 'react-redux'

import { Divider, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core'

import CardDisplay from './card_display'
import Summary from './summary'
const Display = () => {
  const cards = useSelector(state => state.cardsOnHand)
  const history = useSelector(state => state.history)

  return (
    <TableContainer component={Paper}>
      <Grid container spacing={3}>
        {cards.map((card, index) => {
          return (
            <Grid item xs>
              <CardDisplay number={index + 1} face={card.Face} rank={card.Rank} cardvalue={card.Value} />
            </Grid>
          )
        })}
      </Grid>
      <Divider />
      <div>
        <h2>Your game history ...</h2>
      </div>
      <br />
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Draw number</TableCell>
            <TableCell>Final Hand</TableCell>
            <TableCell>Busted?</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {history.map((record, index) => {
            return (
              <TableRow>
                <TableCell>{index + 1}</TableCell>
                <TableCell>
                  <Summary cards={record.Cards} />
                </TableCell>
                <TableCell>{record.Value > 21 ? 'Yes' : 'Not yet'}</TableCell>
              </TableRow>
            )
          })}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default Display
