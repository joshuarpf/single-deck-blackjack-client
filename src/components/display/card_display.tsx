import React from 'react'

import { Card, CardContent, Typography } from '@material-ui/core'

// Styles
import useStyles from './styles'

const CardDisplay = (props) => {
  const classes = useStyles()

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography
          className={classes.title}
          color='textSecondary'
          gutterBottom
        >
          Your Card #{props.number}
        </Typography>
        <Typography variant='h5' component='h2'>
          {props.rank}
        </Typography>
        <Typography className={classes.pos} color='textSecondary'>
          {props.face}
        </Typography>
        <Typography variant='body2' component='p'>
          {props.cardvalue}
        </Typography>
      </CardContent>
    </Card>
  )
}

export default CardDisplay
