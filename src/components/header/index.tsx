import React, { useState } from 'react'

// Material
import {
  AppBar,
  CircularProgress,
  InputBase,
  Toolbar,
  Typography,
} from '@material-ui/core'

import { useDispatch, useSelector } from 'react-redux'

// Interfaces
import { State } from '../../interfaces'

// Actions
import { setPlayer } from '../../actions/game'

import useStyles from './styles'

const Header = () => {
  const [name, setName] = useState('')

  const status = useSelector((state: State) => {
    return state.status
  })

  const indicator = status ? <CircularProgress color='secondary' /> : null

  const classes = useStyles()

  const dispatch = useDispatch()

  const updatePlayerName = (value: string) => {
    setName(value)
    dispatch(setPlayer(value))
  }

  return (
    <AppBar position='static'>
      <Toolbar>
        <Typography className={classes.title} variant='h6'>
          Player
        </Typography>
        <div className={classes.nameForm}>
          <InputBase
            placeholder='Name'
            value={name}
            onChange={(event) => {
              updatePlayerName(event.target.value)
            }}
          />
        </div>
        {indicator}
      </Toolbar>
    </AppBar>
  )
}

export default Header
