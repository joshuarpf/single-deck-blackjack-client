import { DISPLAY_TABLE, DRAW, END_TURN } from '../common/action_types'

import { Action } from '../interfaces'

export const draw = (): Action => ({
  payload: {},
  type: DRAW
})

export const dislayDrawHistoryTable = (data: []): Action => ({
  payload: data,
  type: DISPLAY_TABLE
})

export const endTurn = (playerName: string): Action => ({
  payload: { playerName },
  type: END_TURN
})
