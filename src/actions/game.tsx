import {
  GAME_STATUS,
  SET_CARDS_ON_HAND,
  SET_CURRENT_VALUE,
  SET_PLAYER_NAME,
  SHOW_HISTORY,
  START_GAME
} from '../common/action_types'

import { Action } from '../interfaces'

export const showHistory = (playerName: string): Action => ({
  payload: playerName,
  type: SHOW_HISTORY
})

export const setCardsOnHand = (cards: any[]): Action => ({
  payload: cards,
  type: SET_CARDS_ON_HAND
})

export const setCurrentValue = (value: number): Action => ({
  payload: value,
  type: SET_CURRENT_VALUE
})

export const setPlayer = (name: string): Action => ({
  payload: { playerName: { name } },
  type: SET_PLAYER_NAME
})

export const startGame = (name: string): Action => ({
  payload: { player: { name } },
  type: START_GAME
})

export const toggleBusy = (status: boolean): Action => ({
  payload: status,
  type: GAME_STATUS
})
