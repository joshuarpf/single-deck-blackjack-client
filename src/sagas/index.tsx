import { all, takeEvery } from 'redux-saga/effects'
import { DRAW, END_TURN, SHOW_HISTORY, START_GAME } from '../common/action_types'

// Actions
// Saga Functions
import callToDraw from './functions/draw'
import callToEndTurn from './functions/end_turn'
import callToDisplayHistory from './functions/show_history'
import callToStartGame from './functions/start_game'

/**
 * Main action listener
 */
function* actionWatcher() {
  yield takeEvery(DRAW, callToDraw)
  yield takeEvery(END_TURN, callToEndTurn)
  yield takeEvery(SHOW_HISTORY, callToDisplayHistory)
  yield takeEvery(START_GAME, callToStartGame)
}

/**
 * Root saga
 */
export default function* rootSaga() {
  yield all([actionWatcher()])
}
