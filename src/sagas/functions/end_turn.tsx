import { put } from 'redux-saga/effects'

// Actions
import { showHistory, toggleBusy } from '../../actions/game'

// Modules
import { Game } from '../../modules/Game'

// Interfaces
import { Action } from '../../interfaces'

export default function* callToEndTurn(action: Action) {
  yield put(toggleBusy(true))
  try {
    const APIResult = yield Game.endTurn()
      .then(result => {
        if (result) {
          return result
        }
      })
      .catch(error => {
        // tslint:disable-next-line:no-console
        console.log('Error in Saga -> callToEndTurn: ', error)
      })

    if (APIResult.data) {
      yield put(toggleBusy(false))
      yield put(showHistory(action.payload))
    }

    yield put(toggleBusy(false))
  } catch (error) {
    // tslint:disable-next-line:no-console
    console.log('Error in Saga -> callToEndTurn: ', error)
  }
}
