import { put } from 'redux-saga/effects'

// Actions
import { toggleBusy } from '../../actions/game'
import { dislayDrawHistoryTable } from '../../actions/player'

// Modules
import { Game } from '../../modules/Game'

// Interfaces
import { Action } from '../../interfaces'

export default function* callToDisplayHistory(action: Action) {
  yield put(toggleBusy(true))

  try {
    const APIResult = yield Game.showHistory(action.payload.playerName)
      .then(result => result)
      .catch(error => {
        // tslint:disable-next-line:no-console
        console.log('Exception on Saga -> callToDisplayHistory: ', error)
      })

    if (APIResult && APIResult.data) {
      yield put(dislayDrawHistoryTable(APIResult.data))
      yield put(toggleBusy(false))
    }
  } catch (error) {
    // tslint:disable-next-line:no-console
    console.log('Exception on callToDisplayHistory: ', error)
  }
}
