import { all, put, takeEvery } from 'redux-saga/effects'
import { DRAW, END_TURN, START_GAME } from '../../common/action_types'

// Actions
import { toggleBusy } from '../../actions/game'

// Modules
import { Game } from '../../modules/Game'

// Interfaces
import { Action } from '../../interfaces'

export default function* callToStartGame(action: Action) {
  yield put(toggleBusy(true))

  try {
    const APIResult = yield Game.startGame(action.payload.player.name)
      .then(result => {
        if (result) {
          return result
        }
      })
      .catch(error => {
        console.log('Error in Saga -> callToStartGame: ', error)
      })

    if (APIResult.data) {
      // TODO: do something that will update the state here
      yield put(toggleBusy(false))
    }
  } catch (error) {
    // tslint:disable-next-line:no-console
    console.log('Exception on callToStartGame: ', error)
    yield put(toggleBusy(false))
  }
}
