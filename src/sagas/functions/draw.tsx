import { put } from 'redux-saga/effects'

// Actions
import { setCardsOnHand, setCurrentValue, toggleBusy } from '../../actions/game'

// Modules
import { Game } from '../../modules/Game'

// Interfaces
import { Action } from '../../interfaces'

export default function* callToDraw(action: Action) {
  yield put(toggleBusy(true))
  try {
    const APIResult = yield Game.draw()
      .then(result => {
        if (result) {
          return result
        }
      })
      .catch(error => {
        // tslint:disable-next-line:no-console
        console.log('Error in Saga -> callToDraw: ', error)
      })

    if (APIResult.data) {
      const { CardsOnHand, CurrentValue } = APIResult.data

      yield put(setCardsOnHand(CardsOnHand))
      yield put(setCurrentValue(CurrentValue))

      yield put(toggleBusy(false))
    }
  } catch (error) {
    // tslint:disable-next-line:no-console
    console.log('Exception on Saga -> callToDraw: ', error)
    yield put(toggleBusy(false))
  }
}
