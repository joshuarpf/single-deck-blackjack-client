import {
  GAME_STATUS,
  SET_PLAYER_NAME,
  START_GAME,
} from '../common/action_types'

export interface Action {
  payload: {}
  type: GAME_STATUS || SET_PLAYER_NAME || START_GAME
}

export interface APIResult {
  config: {}
  data: {}
  headers: {}
  request: {}
  status: number
  statusText: string

}

export interface State {
  player: { name: string }
  status: boolean, 
  cardsOnHand: any[],
  currentValue: number,
  history: any[]
}
